<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0">
<context>
    <name>MainWindow</name>
    <message utf8="true">
        <location filename="mainwindow.ui" line="22"/>
        <source>ВПХ Як-40</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="mainwindow.ui" line="53"/>
        <source>Топливо и время</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="73"/>
        <source>S=</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="mainwindow.ui" line="94"/>
        <source>Sзап=</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="mainwindow.ui" line="169"/>
        <location filename="mainwindow.ui" line="345"/>
        <source>Топливо на полёт =</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="mainwindow.ui" line="190"/>
        <source>Время полёта =</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="mainwindow.ui" line="229"/>
        <source>Расчитать</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="280"/>
        <source>HH:mm</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="mainwindow.ui" line="307"/>
        <location filename="mainwindow.ui" line="368"/>
        <location filename="mainwindow.ui" line="539"/>
        <source>кг</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="mainwindow.ui" line="316"/>
        <source>Вес и загрузка</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="mainwindow.ui" line="394"/>
        <source>Расчет веса</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="mainwindow.ui" line="414"/>
        <source>Экипаж =</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="mainwindow.ui" line="437"/>
        <source>Б/пр =</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="mainwindow.ui" line="460"/>
        <source>Техники =</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="mainwindow.ui" line="483"/>
        <source>Загрузка =</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="mainwindow.ui" line="506"/>
        <source>Взлётный вес =</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="mainwindow.ui" line="615"/>
        <source>Скорости</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="mainwindow.ui" line="645"/>
        <source>Стабилизатор</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="mainwindow.ui" line="694"/>
        <source>Файл</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="mainwindow.ui" line="701"/>
        <source>Помощь</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="mainwindow.ui" line="712"/>
        <source>Сохранить</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="mainwindow.ui" line="717"/>
        <source>Выход</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="mainwindow.ui" line="722"/>
        <source>О программе</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="mainwindow.ui" line="727"/>
        <source>О Qt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="88"/>
        <source>? ?????????</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="89"/>
        <source>?????? ??? ??? ??-40
 ??? ????</source>
        <translation type="unfinished"></translation>
    </message>
	</context>
	<context>
    <name>QMessageBox</name>
    <message>
        <source>OK</source>
        <translation>Закрыть</translation>
    </message>
    <message>
        <source>&lt;h3&gt;About Qt&lt;/h3&gt;&lt;p&gt;This program uses Qt version %1.&lt;/p&gt;</source>
        <translation>&lt;h3&gt;О Qt&lt;/h3&gt;&lt;p&gt;Данная программа использует Qt версии %1.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;Qt is a C++ toolkit for cross-platform application development.&lt;/p&gt;&lt;p&gt;Qt provides single-source portability across MS&amp;nbsp;Windows, Mac&amp;nbsp;OS&amp;nbsp;X, Linux, and all major commercial Unix variants. Qt is also available for embedded devices as Qt for Embedded Linux and Qt for Windows CE.&lt;/p&gt;&lt;p&gt;Qt is available under three different licensing options designed to accommodate the needs of our various users.&lt;/p&gt;&lt;p&gt;Qt licensed under our commercial license agreement is appropriate for development of proprietary/commercial software where you do not want to share any source code with third parties or otherwise cannot comply with the terms of the GNU LGPL version 2.1 or GNU GPL version 3.0.&lt;/p&gt;&lt;p&gt;Qt licensed under the GNU LGPL version 2.1 is appropriate for the development of Qt applications (proprietary or open source) provided you can comply with the terms and conditions of the GNU LGPL version 2.1.&lt;/p&gt;&lt;p&gt;Qt licensed under the GNU General Public License version 3.0 is appropriate for the development of Qt applications where you wish to use such applications in combination with software subject to the terms of the GNU GPL version 3.0 or where you are otherwise willing to comply with the terms of the GNU GPL version 3.0.&lt;/p&gt;&lt;p&gt;Please see &lt;a href=&quot;http://qt.nokia.com/products/licensing&quot;&gt;qt.nokia.com/products/licensing&lt;/a&gt; for an overview of Qt licensing.&lt;/p&gt;&lt;p&gt;Copyright (C) 2012 Nokia Corporation and/or its subsidiary(-ies).&lt;/p&gt;&lt;p&gt;Qt is a Nokia product. See &lt;a href=&quot;http://qt.nokia.com/&quot;&gt;qt.nokia.com&lt;/a&gt; for more information.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Qt - это инструментарий для разработки кроссплатформенных приложений на C++.&lt;/p&gt;&lt;p&gt;Qt предоставляет совместимость на уровне исходных текстов между MS&amp;nbsp;Windows, Mac&amp;nbsp;OS&amp;nbsp;X, Linux и всеми популярными коммерческими вариантами Unix. Также Qt доступна для встраиваемых устройств в виде Qt для Embedded Linux и Qt для Windows CE.&lt;/p&gt;&lt;p&gt;Qt доступна под тремя различными лицензиями, разработанными для удовлетворения различных требований.&lt;/p&gt;&lt;p&gt;Qt под нашей коммерческой лицензией предназначена для развития проприетарного/коммерческого программного обеспечения, когда Вы не желаете предоставлять исходные тексты третьим сторонам, или в случае невозможности принятия условий лицензий GNU LGPL версии 2.1 или GNU GPL версии 3.0.&lt;/p&gt;&lt;p&gt;Qt под лицензией GNU LGPL версии 2.1 предназначена для разработки программного обеспечения с открытыми исходными текстами или коммерческого программного обеспечения при соблюдении условий лицензии GNU LGPL версии 2.1.&lt;/p&gt;&lt;p&gt;Qt под лицензией GNU General Public License версии 3.0 предназначена для разработки программных приложений в тех случаях, когда Вы хотели бы использовать такие приложения в сочетании с программным обеспечением на условиях лицензии GNU GPL с версии 3.0 или если Вы готовы соблюдать условия лицензии GNU GPL версии 3.0.&lt;/p&gt;&lt;p&gt;Обратитесь к &lt;a href=&quot;http://qt.nokia.com/products/licensing&quot;&gt;qt.nokia.com/products/licensing&lt;/a&gt; для обзора лицензий Qt.&lt;/p&gt;&lt;p&gt;Copyright (C) 2012 Корпорация Nokia и/или её дочерние подразделения.&lt;/p&gt;&lt;p&gt;Qt - продукт компании Nokia. Обратитесь к &lt;a href=&quot;http://qt.nokia.com/&quot;&gt;qt.nokia.com&lt;/a&gt; для получения дополнительной информации.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>About Qt</source>
        <translation>О Qt</translation>
    </message>
    <message>
        <source>Help</source>
        <translation>Справка</translation>
    </message>
    <message>
        <source>Show Details...</source>
        <translation>Показать подробности...</translation>
    </message>
    <message>
        <source>Hide Details...</source>
        <translation>Скрыть подробности...</translation>
    </message>
</context>
</TS>

﻿#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QTextCodec>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    trIcon = new QSystemTrayIcon();  //инициализируем объект
    trIcon->setIcon(QIcon("img/accessories-calculator.png"));  //устанавливаем иконку
    trIcon->show();  //отображаем объект

    connect(ui->aboutApp, SIGNAL(triggered()), this, SLOT(about()));
    connect(ui->about_Qt, SIGNAL(activated()), qApp, SLOT(aboutQt()));
    connect(ui->exitApp, SIGNAL(triggered()), this, SLOT(closeapp()));
    connect(ui->butRasch, SIGNAL(clicked()), this, SLOT(fuel()));
    connect(ui->butRaschVes, SIGNAL(clicked()), this, SLOT(ves()));
    connect(trIcon,SIGNAL(activated(QSystemTrayIcon::ActivationReason)),this,SLOT(showHide(QSystemTrayIcon::ActivationReason)));

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::changeEvent(QEvent *e)
{
    QMainWindow::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void MainWindow::fuel()
{
    int S = ui->spinBoxS->value();
    int Salt = ui->spinBoxSalt->value();
    float Tpsum = ((float)S+(float)Salt)/420; //Время полета для общего топлива
    float Tp = (float)S/(float)420; //Время полета до ап
    int Hh = (int)Tp;
    int Mm = (int)((Tp- Hh)*60);
    QTime n;
    n.setHMS(Hh,Mm,00,00);
    int Gf = (((Tpsum*1200)*1.03)+600);
    ui->lineEditG->setText(QString::number(Gf));//Вывод общего топлива
    ui->lineEditG_2->setText(QString::number(Gf));//Вывод общего топлива для загрузки
    ui->timeEdit->setTime(n);//Вывод времени полёта
}

void MainWindow::ves()
{
    int Gpust = 11245;
    int Gsl = 80;
    int Gfull = ui->lineEditG_2->text().toInt();
    int vEk = 80;
    int vPr = 75;
    int vTeh = 75;
    int vPass = 80;
    int Gvzl = Gpust+Gsl+Gfull+(ui->spinEk->value()*vEk)+(ui->spinPr->value()*vPr)+(ui->spinTeh->value()*vTeh)+(ui->spinZagr->value()*vPass)-65;
    ui->lineVzl->setText(QString::number(Gvzl));

}

void MainWindow::paintEvent(QPaintEvent *)
//void MainWindow::paintEvent()
{
    QGraphicsScene * scen = new QGraphicsScene();
        QPixmap * pix = new QPixmap();
        pix->load("img/1.png");
        scen->addPixmap(*pix);
        ui->graphicsView->setScene(scen);
        QGraphicsScene * scenC = new QGraphicsScene();
            QPixmap * pixC = new QPixmap();
            pixC->load("img/3.png");
            scenC->addPixmap(*pixC);
            ui->graphicsView_CAX->setScene(scenC);
        //ui->Mnemo->setScene(scen);//Mnemo - компонент QGraphicsView
//QImage img("img/1.png"); // загружаем картинку
//QPainter painter(this); // определяем объект painter, который обеспечивает рисование
//painter.drawImage(0,0, img.scaled(this->size()));
}

void MainWindow::showHide(QSystemTrayIcon::ActivationReason r) {
    if (r == QSystemTrayIcon::Trigger){  //если нажато левой кнопкой продолжаем
    if (!this->isVisible()) {  //если окно было не видимо - отображаем его
        this->show();
    } else {
        this->hide();  //иначе скрываем
    }
    }
}

void MainWindow::about()
{
   QMessageBox::about(this, tr("О программе"),
            tr("Расчёт ВПХ для Як-40\n ООО СКОЛ"));
}

void MainWindow::closeapp()
{

    close();
}

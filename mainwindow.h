#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtGui>
#include <QPicture>
#include <QImage>
#include <QPainter>
#include <QSystemTrayIcon>


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    QAction *aboutApp;
    QAction *exitApp;
    QPushButton *butRasch;
    QPushButton *butRaschVes;
    QSpinBox *spinBoxS;
    QSpinBox *spinBoxSalt;
    QSpinBox *vEk;
    QSpinBox *vPr;
    QSpinBox *vTeh;
    QSpinBox *vPass;
    QLineEdit *lineEditG;
    QLineEdit *lineEditG_2;
    QLineEdit *lineVzl;
    QWidget *tabGraf1;
    QGraphicsView *graphicsView;
    QListView *listView;



protected:
void changeEvent(QEvent *e);
void paintEvent(QPaintEvent *); // пееопределение виртуальной функции

public slots:
    void fuel();
    void ves();
    void closeapp();

private slots:
    void about();
    void showHide(QSystemTrayIcon::ActivationReason);

private:
    Ui::MainWindow *ui;
    QSystemTrayIcon *trIcon;
};

#endif // MAINWINDOW_H

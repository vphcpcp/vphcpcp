#include <QtGui/QApplication>
#include "mainwindow.h"


int main(int argc, char *argv[])
{
    QTextCodec *cyrillicCodec = QTextCodec::codecForName("UTF-8");
    QTextCodec::setCodecForTr(cyrillicCodec);
    QTextCodec::setCodecForLocale(cyrillicCodec);
    QTextCodec::setCodecForCStrings(cyrillicCodec);

    QApplication a(argc, argv);
    QTranslator myTranslator;
    myTranslator.load(QLocale::system().name());
    a.installTranslator(&myTranslator);
    MainWindow w;
    w.show();
    //QApplication::setQuitOnLastWindowClosed(false);/**Чтобы приложение не закрывалось при закрытии последнего окна, а оставалось "висеть" в трее*/
    
    return a.exec();
}

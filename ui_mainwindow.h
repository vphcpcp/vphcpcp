/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created: Tue 13. Nov 09:10:26 2012
**      by: Qt User Interface Compiler version 4.8.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QGraphicsView>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QMainWindow>
#include <QtGui/QMenu>
#include <QtGui/QMenuBar>
#include <QtGui/QPushButton>
#include <QtGui/QSpinBox>
#include <QtGui/QStatusBar>
#include <QtGui/QTabWidget>
#include <QtGui/QTimeEdit>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *save;
    QAction *exitApp;
    QAction *aboutApp;
    QAction *about_Qt;
    QWidget *centralwidget;
    QVBoxLayout *verticalLayout;
    QTabWidget *tabWidget;
    QWidget *fuel_time;
    QLabel *labelS;
    QLabel *labelSalt;
    QSpinBox *spinBoxS;
    QSpinBox *spinBoxSalt;
    QLabel *labelGt;
    QLabel *labelT;
    QLineEdit *lineEditG;
    QPushButton *butRasch;
    QTimeEdit *timeEdit;
    QLabel *labelKg;
    QWidget *wight_pass;
    QLineEdit *lineEditG_2;
    QLabel *labelGt_2;
    QLabel *labelKg_2;
    QPushButton *butRaschVes;
    QLabel *labelEk;
    QLabel *labelPr;
    QLabel *labelTeh;
    QLabel *labelZagr;
    QLabel *labelGvzl;
    QLineEdit *lineVzl;
    QLabel *labelKg_Vzl;
    QSpinBox *spinEk;
    QSpinBox *spinPr;
    QSpinBox *spinTeh;
    QSpinBox *spinZagr;
    QWidget *tabSpeed;
    QGraphicsView *graphicsView;
    QWidget *tabCAX;
    QGraphicsView *graphicsView_CAX;
    QMenuBar *menuBar;
    QMenu *menuFile;
    QMenu *menuHelp;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(797, 507);
        QFont font;
        font.setFamily(QString::fromUtf8("Arial"));
        font.setPointSize(10);
        font.setBold(true);
        font.setWeight(75);
        MainWindow->setFont(font);
        MainWindow->setStyleSheet(QString::fromUtf8(""));
        save = new QAction(MainWindow);
        save->setObjectName(QString::fromUtf8("save"));
        exitApp = new QAction(MainWindow);
        exitApp->setObjectName(QString::fromUtf8("exitApp"));
        aboutApp = new QAction(MainWindow);
        aboutApp->setObjectName(QString::fromUtf8("aboutApp"));
        about_Qt = new QAction(MainWindow);
        about_Qt->setObjectName(QString::fromUtf8("about_Qt"));
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        centralwidget->setStyleSheet(QString::fromUtf8(""));
        verticalLayout = new QVBoxLayout(centralwidget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        tabWidget = new QTabWidget(centralwidget);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        tabWidget->setFont(font);
        tabWidget->setStyleSheet(QString::fromUtf8(""));
        fuel_time = new QWidget();
        fuel_time->setObjectName(QString::fromUtf8("fuel_time"));
        fuel_time->setStyleSheet(QString::fromUtf8(""));
        labelS = new QLabel(fuel_time);
        labelS->setObjectName(QString::fromUtf8("labelS"));
        labelS->setGeometry(QRect(30, 20, 21, 16));
        labelS->setFont(font);
        labelSalt = new QLabel(fuel_time);
        labelSalt->setObjectName(QString::fromUtf8("labelSalt"));
        labelSalt->setGeometry(QRect(10, 60, 42, 16));
        labelSalt->setFont(font);
        spinBoxS = new QSpinBox(fuel_time);
        spinBoxS->setObjectName(QString::fromUtf8("spinBoxS"));
        spinBoxS->setGeometry(QRect(60, 20, 81, 22));
        spinBoxS->setFont(font);
        spinBoxS->setMinimum(150);
        spinBoxS->setMaximum(2400);
        spinBoxS->setSingleStep(50);
        spinBoxSalt = new QSpinBox(fuel_time);
        spinBoxSalt->setObjectName(QString::fromUtf8("spinBoxSalt"));
        spinBoxSalt->setGeometry(QRect(60, 60, 81, 20));
        spinBoxSalt->setFont(font);
        spinBoxSalt->setMinimum(50);
        spinBoxSalt->setMaximum(1000);
        spinBoxSalt->setSingleStep(10);
        labelGt = new QLabel(fuel_time);
        labelGt->setObjectName(QString::fromUtf8("labelGt"));
        labelGt->setGeometry(QRect(180, 20, 141, 16));
        labelGt->setFont(font);
        labelT = new QLabel(fuel_time);
        labelT->setObjectName(QString::fromUtf8("labelT"));
        labelT->setGeometry(QRect(200, 60, 111, 16));
        labelT->setFont(font);
        lineEditG = new QLineEdit(fuel_time);
        lineEditG->setObjectName(QString::fromUtf8("lineEditG"));
        lineEditG->setGeometry(QRect(330, 20, 51, 20));
        lineEditG->setFont(font);
        butRasch = new QPushButton(fuel_time);
        butRasch->setObjectName(QString::fromUtf8("butRasch"));
        butRasch->setGeometry(QRect(20, 90, 85, 27));
        butRasch->setFont(font);
        timeEdit = new QTimeEdit(fuel_time);
        timeEdit->setObjectName(QString::fromUtf8("timeEdit"));
        timeEdit->setEnabled(true);
        timeEdit->setGeometry(QRect(330, 60, 61, 24));
        timeEdit->setFont(font);
        timeEdit->setContextMenuPolicy(Qt::NoContextMenu);
        timeEdit->setWrapping(false);
        timeEdit->setFrame(true);
        timeEdit->setAlignment(Qt::AlignCenter);
        timeEdit->setReadOnly(false);
        timeEdit->setButtonSymbols(QAbstractSpinBox::NoButtons);
        timeEdit->setAccelerated(false);
        timeEdit->setKeyboardTracking(false);
        timeEdit->setCurrentSection(QDateTimeEdit::HourSection);
        timeEdit->setCalendarPopup(false);
        labelKg = new QLabel(fuel_time);
        labelKg->setObjectName(QString::fromUtf8("labelKg"));
        labelKg->setGeometry(QRect(390, 25, 46, 13));
        labelKg->setFont(font);
        labelKg->setFrameShape(QFrame::NoFrame);
        labelKg->setAlignment(Qt::AlignCenter);
        tabWidget->addTab(fuel_time, QString());
        wight_pass = new QWidget();
        wight_pass->setObjectName(QString::fromUtf8("wight_pass"));
        lineEditG_2 = new QLineEdit(wight_pass);
        lineEditG_2->setObjectName(QString::fromUtf8("lineEditG_2"));
        lineEditG_2->setGeometry(QRect(149, 17, 51, 20));
        labelGt_2 = new QLabel(wight_pass);
        labelGt_2->setObjectName(QString::fromUtf8("labelGt_2"));
        labelGt_2->setGeometry(QRect(13, 20, 131, 16));
        QFont font1;
        font1.setPointSize(10);
        font1.setBold(true);
        font1.setWeight(75);
        labelGt_2->setFont(font1);
        labelKg_2 = new QLabel(wight_pass);
        labelKg_2->setObjectName(QString::fromUtf8("labelKg_2"));
        labelKg_2->setGeometry(QRect(206, 20, 31, 16));
        labelKg_2->setFont(font1);
        labelKg_2->setFrameShape(QFrame::NoFrame);
        labelKg_2->setAlignment(Qt::AlignCenter);
        butRaschVes = new QPushButton(wight_pass);
        butRaschVes->setObjectName(QString::fromUtf8("butRaschVes"));
        butRaschVes->setGeometry(QRect(20, 280, 91, 23));
        QFont font2;
        font2.setFamily(QString::fromUtf8("Arial"));
        font2.setBold(true);
        font2.setWeight(75);
        butRaschVes->setFont(font2);
        butRaschVes->setMouseTracking(false);
        labelEk = new QLabel(wight_pass);
        labelEk->setObjectName(QString::fromUtf8("labelEk"));
        labelEk->setGeometry(QRect(10, 60, 71, 16));
        labelEk->setFont(font1);
        labelEk->setAlignment(Qt::AlignCenter);
        labelPr = new QLabel(wight_pass);
        labelPr->setObjectName(QString::fromUtf8("labelPr"));
        labelPr->setGeometry(QRect(12, 92, 51, 16));
        labelPr->setFont(font1);
        labelPr->setAlignment(Qt::AlignCenter);
        labelTeh = new QLabel(wight_pass);
        labelTeh->setObjectName(QString::fromUtf8("labelTeh"));
        labelTeh->setGeometry(QRect(13, 130, 71, 16));
        labelTeh->setFont(font1);
        labelTeh->setAlignment(Qt::AlignCenter);
        labelZagr = new QLabel(wight_pass);
        labelZagr->setObjectName(QString::fromUtf8("labelZagr"));
        labelZagr->setGeometry(QRect(16, 166, 71, 16));
        labelZagr->setFont(font1);
        labelZagr->setAlignment(Qt::AlignCenter);
        labelGvzl = new QLabel(wight_pass);
        labelGvzl->setObjectName(QString::fromUtf8("labelGvzl"));
        labelGvzl->setGeometry(QRect(17, 201, 111, 16));
        labelGvzl->setFont(font1);
        lineVzl = new QLineEdit(wight_pass);
        lineVzl->setObjectName(QString::fromUtf8("lineVzl"));
        lineVzl->setGeometry(QRect(150, 198, 51, 20));
        labelKg_Vzl = new QLabel(wight_pass);
        labelKg_Vzl->setObjectName(QString::fromUtf8("labelKg_Vzl"));
        labelKg_Vzl->setGeometry(QRect(207, 201, 31, 16));
        labelKg_Vzl->setFont(font1);
        labelKg_Vzl->setFrameShape(QFrame::NoFrame);
        labelKg_Vzl->setAlignment(Qt::AlignCenter);
        spinEk = new QSpinBox(wight_pass);
        spinEk->setObjectName(QString::fromUtf8("spinEk"));
        spinEk->setGeometry(QRect(150, 50, 42, 22));
        spinEk->setMinimum(3);
        spinEk->setMaximum(5);
        spinPr = new QSpinBox(wight_pass);
        spinPr->setObjectName(QString::fromUtf8("spinPr"));
        spinPr->setGeometry(QRect(150, 84, 42, 22));
        spinPr->setMinimum(1);
        spinPr->setMaximum(3);
        spinTeh = new QSpinBox(wight_pass);
        spinTeh->setObjectName(QString::fromUtf8("spinTeh"));
        spinTeh->setGeometry(QRect(150, 122, 42, 22));
        spinTeh->setMinimum(0);
        spinTeh->setMaximum(2);
        spinZagr = new QSpinBox(wight_pass);
        spinZagr->setObjectName(QString::fromUtf8("spinZagr"));
        spinZagr->setGeometry(QRect(150, 160, 42, 22));
        spinZagr->setMinimum(0);
        spinZagr->setMaximum(10);
        tabWidget->addTab(wight_pass, QString());
        tabSpeed = new QWidget();
        tabSpeed->setObjectName(QString::fromUtf8("tabSpeed"));
        tabSpeed->setAutoFillBackground(false);
        graphicsView = new QGraphicsView(tabSpeed);
        graphicsView->setObjectName(QString::fromUtf8("graphicsView"));
        graphicsView->setGeometry(QRect(-5, 1, 781, 431));
        QBrush brush(QColor(0, 0, 0, 255));
        brush.setStyle(Qt::NoBrush);
        graphicsView->setBackgroundBrush(brush);
        graphicsView->setDragMode(QGraphicsView::NoDrag);
        graphicsView->setResizeAnchor(QGraphicsView::AnchorViewCenter);
        tabWidget->addTab(tabSpeed, QString());
        tabCAX = new QWidget();
        tabCAX->setObjectName(QString::fromUtf8("tabCAX"));
        graphicsView_CAX = new QGraphicsView(tabCAX);
        graphicsView_CAX->setObjectName(QString::fromUtf8("graphicsView_CAX"));
        graphicsView_CAX->setGeometry(QRect(0, 0, 781, 431));
        graphicsView_CAX->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
        graphicsView_CAX->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
        graphicsView_CAX->setDragMode(QGraphicsView::NoDrag);
        graphicsView_CAX->setTransformationAnchor(QGraphicsView::AnchorViewCenter);
        graphicsView_CAX->setResizeAnchor(QGraphicsView::AnchorViewCenter);
        graphicsView_CAX->setViewportUpdateMode(QGraphicsView::MinimalViewportUpdate);
        graphicsView_CAX->setRubberBandSelectionMode(Qt::IntersectsItemShape);
        tabWidget->addTab(tabCAX, QString());

        verticalLayout->addWidget(tabWidget);

        MainWindow->setCentralWidget(centralwidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 797, 19));
        menuFile = new QMenu(menuBar);
        menuFile->setObjectName(QString::fromUtf8("menuFile"));
        menuHelp = new QMenu(menuBar);
        menuHelp->setObjectName(QString::fromUtf8("menuHelp"));
        MainWindow->setMenuBar(menuBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        MainWindow->setStatusBar(statusBar);

        menuBar->addAction(menuFile->menuAction());
        menuBar->addAction(menuHelp->menuAction());
        menuFile->addAction(save);
        menuFile->addAction(exitApp);
        menuHelp->addAction(aboutApp);
        menuHelp->addAction(about_Qt);

        retranslateUi(MainWindow);

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "\320\222\320\237\320\245 \320\257\320\272-40", 0, QApplication::UnicodeUTF8));
        save->setText(QApplication::translate("MainWindow", "\320\241\320\276\321\205\321\200\320\260\320\275\320\270\321\202\321\214", 0, QApplication::UnicodeUTF8));
        exitApp->setText(QApplication::translate("MainWindow", "\320\222\321\213\321\205\320\276\320\264", 0, QApplication::UnicodeUTF8));
        aboutApp->setText(QApplication::translate("MainWindow", "\320\236 \320\277\321\200\320\276\320\263\321\200\320\260\320\274\320\274\320\265", 0, QApplication::UnicodeUTF8));
        about_Qt->setText(QApplication::translate("MainWindow", "\320\236 Qt", 0, QApplication::UnicodeUTF8));
        labelS->setText(QApplication::translate("MainWindow", "S=", 0, QApplication::UnicodeUTF8));
        labelSalt->setText(QApplication::translate("MainWindow", "S\320\267\320\260\320\277=", 0, QApplication::UnicodeUTF8));
        labelGt->setText(QApplication::translate("MainWindow", "\320\242\320\276\320\277\320\273\320\270\320\262\320\276 \320\275\320\260 \320\277\320\276\320\273\321\221\321\202 =", 0, QApplication::UnicodeUTF8));
        labelT->setText(QApplication::translate("MainWindow", "\320\222\321\200\320\265\320\274\321\217 \320\277\320\276\320\273\321\221\321\202\320\260 =", 0, QApplication::UnicodeUTF8));
        butRasch->setText(QApplication::translate("MainWindow", "\320\240\320\260\321\201\321\207\320\270\321\202\320\260\321\202\321\214", 0, QApplication::UnicodeUTF8));
        timeEdit->setDisplayFormat(QApplication::translate("MainWindow", "HH:mm", 0, QApplication::UnicodeUTF8));
        labelKg->setText(QApplication::translate("MainWindow", "\320\272\320\263", 0, QApplication::UnicodeUTF8));
        tabWidget->setTabText(tabWidget->indexOf(fuel_time), QApplication::translate("MainWindow", "\320\242\320\276\320\277\320\273\320\270\320\262\320\276 \320\270 \320\262\321\200\320\265\320\274\321\217", 0, QApplication::UnicodeUTF8));
        labelGt_2->setText(QApplication::translate("MainWindow", "\320\242\320\276\320\277\320\273\320\270\320\262\320\276 \320\275\320\260 \320\277\320\276\320\273\321\221\321\202 =", 0, QApplication::UnicodeUTF8));
        labelKg_2->setText(QApplication::translate("MainWindow", "\320\272\320\263", 0, QApplication::UnicodeUTF8));
        butRaschVes->setText(QApplication::translate("MainWindow", "\320\240\320\260\321\201\321\207\320\265\321\202 \320\262\320\265\321\201\320\260", 0, QApplication::UnicodeUTF8));
        labelEk->setText(QApplication::translate("MainWindow", "\320\255\320\272\320\270\320\277\320\260\320\266 =", 0, QApplication::UnicodeUTF8));
        labelPr->setText(QApplication::translate("MainWindow", "\320\221/\320\277\321\200 =", 0, QApplication::UnicodeUTF8));
        labelTeh->setText(QApplication::translate("MainWindow", "\320\242\320\265\321\205\320\275\320\270\320\272\320\270 =", 0, QApplication::UnicodeUTF8));
        labelZagr->setText(QApplication::translate("MainWindow", "\320\227\320\260\320\263\321\200\321\203\320\267\320\272\320\260 =", 0, QApplication::UnicodeUTF8));
        labelGvzl->setText(QApplication::translate("MainWindow", "\320\222\320\267\320\273\321\221\321\202\320\275\321\213\320\271 \320\262\320\265\321\201 =", 0, QApplication::UnicodeUTF8));
        labelKg_Vzl->setText(QApplication::translate("MainWindow", "\320\272\320\263", 0, QApplication::UnicodeUTF8));
        tabWidget->setTabText(tabWidget->indexOf(wight_pass), QApplication::translate("MainWindow", "\320\222\320\265\321\201 \320\270 \320\267\320\260\320\263\321\200\321\203\320\267\320\272\320\260", 0, QApplication::UnicodeUTF8));
        tabWidget->setTabText(tabWidget->indexOf(tabSpeed), QApplication::translate("MainWindow", "\320\241\320\272\320\276\321\200\320\276\321\201\321\202\320\270", 0, QApplication::UnicodeUTF8));
        tabWidget->setTabText(tabWidget->indexOf(tabCAX), QApplication::translate("MainWindow", "\320\241\321\202\320\260\320\261\320\270\320\273\320\270\320\267\320\260\321\202\320\276\321\200", 0, QApplication::UnicodeUTF8));
        menuFile->setTitle(QApplication::translate("MainWindow", "\320\244\320\260\320\271\320\273", 0, QApplication::UnicodeUTF8));
        menuHelp->setTitle(QApplication::translate("MainWindow", "\320\237\320\276\320\274\320\276\321\211\321\214", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
